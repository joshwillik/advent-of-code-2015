#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util/array.c"
#define PUZZLE_INPUT "day8.txt"
#define MAX_LINE 256

Array *parse_input() {
  Array *lines = array_new();
  FILE *f = fopen(PUZZLE_INPUT, "r");
  char line[MAX_LINE];
  while (fgets(line, MAX_LINE, f)) {
    char *new_line = calloc(strlen(line) + 1, sizeof(char));
    strcpy(new_line, line);
    array_push(lines, (intptr_t) new_line);
  }
  fclose(f);
  return lines;
}

int eat_token(char *str) {
  if (str[0] == '\\') {
     if (str[1] == '"' || str[1] == '\\') {
       return 2;
     }
     return 4; // eat token in form \x00
  }
  return 1;
}

int reducelen(char *str) {
  char *end = str + strlen(str);
  int length = 0;
  while (str < end) {
    length += 1;
    str += eat_token(str);
  }
  return length - 2 /* wrapping quotes */;
}

int expandlen(char *str) {
  int length = 0;
  for (size_t i = 0; str[i] != '\0'; i++) {
    length += (str[i] == '\\' || str[i] == '"') ? 2 : 1;
  }
  return length + 2 /* wrapping quotes */;
}

int main() {
  Array *lines = parse_input();
  int reduce_diff = 0, expand_diff = 0;
  for (size_t i = 0; i < lines->length; i++) {
    char *line = (char*) lines->items[i];
    reduce_diff += strlen(line) - reducelen(line);
    expand_diff += expandlen(line) - strlen(line);
  }
  printf("Reduce diff: %d\n", reduce_diff);
  printf("Expand diff: %d\n", expand_diff);
  array_free(lines, NULL);
  return 0;
}
