#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include "util/array.c"
#define PUZZLE_INPUT "day19.txt"
#define MAX_LINE 60
#define PROTEIN_LENGTH 1024
#define STARTING_PROTEIN "e"

typedef struct Replacement {
  char from[MAX_LINE];
  char to[MAX_LINE];
} Replacement;

void parse_input(Array *replacements, char *protein) {
  FILE *f = fopen(PUZZLE_INPUT, "r");
  char line[MAX_LINE];
  while(fgets(line, MAX_LINE, f)) {
    if (line[0] == '\n') {
      break;
    }
    Replacement *r = malloc(sizeof(Replacement));
    int scanned = sscanf(line, "%s => %s", r->from, r->to);
    if (scanned != 2) {
      fprintf(stderr, "Could not parse line: %s\n", line);
      exit(1);
    }
    array_push(replacements, (intptr_t) r);
  }
  fgets(protein, PROTEIN_LENGTH, f);
  protein[strlen(protein) - 1] = '\0';
  fclose(f);
}

bool array_contains(Array *set, char *new_item) {
  for (size_t i = 0; i < set->length; i++) {
    char *item = (char*) set->items[i];
    if (!strcmp(item, new_item)) {
      return true;
    }
  }
  return false;
}

int indexof(char *a, char *b) {
  char *start = strstr(a, b);
  if (!start) {
    return -1;
  }
  return start - a;
}

void replace_protein(const char *src, char *from, char *to, size_t replace_at,
  char *dest)
{
  for (size_t i = 0; i < replace_at; i++) {
    *dest++ = *src++;
  }
  src += strlen(from);
  for (size_t i = 0, end = strlen(to); i < end; i++) {
    *dest++ = to[i];
  }
  while ((*dest++ = *src++));
}

size_t distinct_proteins(char *protein, Array *replacements) {
  Array *distinct = array_new();
  for (size_t i = 0; i < replacements->length; i++) {
    Replacement *r = (Replacement*) replacements->items[i];
    size_t offset = 0;
    while (true) {
      int index = indexof(protein + offset, r->from);
      if (index == -1) {
        break;
      }
      offset += index + 1;
      index = offset - 1;
      char *new_protein = malloc(sizeof(char) * PROTEIN_LENGTH);
      replace_protein(protein, r->from, r->to, index, new_protein);
      if (array_contains(distinct, new_protein)) {
        free(new_protein);
      } else {
        array_push(distinct, (intptr_t) new_protein);
      }
    }
  }
  size_t n = distinct->length;
  array_free(distinct, NULL);
  return n;
}

void swap_str(char **a, char **b) {
  char *temp = *a;
  *a = *b;
  *b = temp;
}

size_t steps_needed(char *from, char *to, Array *replacements) {
  char *str = malloc(sizeof(char) * strlen(to) + 1);
  char *tmp = malloc(sizeof(char) * strlen(to) + 1);
  strcpy(str, to);
  size_t steps = 0;
  do {
    for (size_t i = 0; i < replacements->length; i++) {
      Replacement r = *(Replacement*) replacements->items[i];
      int index = indexof(str, r.to);
      if (index == -1) {
        continue;
      }
      replace_protein(str, r.to, r.from, index, tmp);
      swap_str(&str, &tmp);
      steps += 1;
      printf("Replaced %s -> %s to (%zu)%s\n", r.to, r.from, strlen(str), str);
      break;
    }
  } while (strcmp(str, from));
  free(str);
  free(tmp);
  return steps;
}

void swap_p(intptr_t *a, intptr_t *b) {
  intptr_t temp = *a;
  *a = *b;
  *b = temp;
}


void sort_replacements(Array *replacements) {
  bool swapped;
  size_t n = replacements->length;
  intptr_t *items = replacements->items;
  do {
    swapped = false;
    for (size_t i = 1; i < n - 1; i++) {
      size_t a_len = strlen(((Replacement*)items[i - 1])->to);
      size_t b_len = strlen(((Replacement*)items[i])->to);
      if (a_len < b_len) {
        swap_p(items + i - 1, items + i);
        swapped = true;
      }
    }
    n--;
  } while(swapped);
}

int main() {
  Array *replacements = array_new();
  char protein[PROTEIN_LENGTH];
  parse_input(replacements, protein);
  sort_replacements(replacements);
  for (size_t i = 0; i < replacements->length; i++) {
    Replacement *r = (Replacement*) replacements->items[i];
    printf("Replace %s -> %s\n", r->from, r->to);
  }
  size_t n_replacements = distinct_proteins(protein, replacements);
  printf("Protein: %s can turn into %lu proteins\n", protein, n_replacements);
  size_t n_steps = steps_needed(STARTING_PROTEIN, protein, replacements);
  printf("Protein %s needs %zu steps to get to %s\n", STARTING_PROTEIN, n_steps,
    protein);
  array_free(replacements, NULL);
  return 0;
}
