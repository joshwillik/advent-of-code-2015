#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include "util/array.c"
#define SHOP_INPUT "day21_shop.txt"
#define BOSS_INPUT "day21_boss.txt"
#define MAX_LINE 60
#define DEFAULT_GOLD 0
#define MIN(a, b) (((a) > (b)) ? (b) : (a))
#define MAX(a, b) (((a) < (b)) ? (b) : (a))

typedef struct Stats {
  unsigned int cost;
  unsigned int damage;
  unsigned int armor;
} Stats;

typedef struct Entity {
  unsigned int health;
  Stats stats;
} Entity;

typedef struct Item {
  char name[MAX_LINE];
  Stats stats;
} Item;

bool is_whitespace(char c) {
  return c == ' ' || c == '\t';
}

void print_item(Item i) {
  printf("%s: cost=%u damage=%u armor=%u\n", i.name, i.stats.cost,
    i.stats.damage, i.stats.armor);
}

FILE *safe_fopen(char *path, char *perm) {
  FILE *f = fopen(path, perm);
  if (!f) {
    fprintf(stderr, "Cannot open file: %s(%s)\n", path, perm);
    exit(1);
  }
  return f;
}

void parse_shop(Array *weapons, Array *armors, Array *rings) {
  FILE *f = safe_fopen(SHOP_INPUT, "r");
  if (!f) {
    printf("Missing file %s\n", SHOP_INPUT);
    exit(1);
  }
  char line[MAX_LINE];
  Array *items = NULL;
  while (fgets(line, MAX_LINE, f)) {
    if (!strcmp(line, "\n")) {
      continue;
    }
    char category[MAX_LINE];
    if ((sscanf(line, "%s:", category) == 1) &&
      (category[strlen(category) - 1] == ':'))
    {
      if (!strcmp("Weapons:", category)) {
        items = weapons;
      } else if (!strcmp("Armor:", category)) {
        items = armors;
      } else if (!strcmp("Rings:", category)) {
        items = rings;
      }
      continue;
    }
    assert(items != NULL);
    Item *item = calloc(1, sizeof(Item));
    if (items == rings) {
      char modifier[16];
      size_t tokens = sscanf(line, "%s %s %u %u %u", item->name, modifier,
        &item->stats.cost, &item->stats.damage, &item->stats.armor);
      if (tokens != 5) {
        fprintf(stderr, "Only able to scan %zu in line: %s\n", tokens, line);
        exit(1);
      }
      strcat(item->name, " ");
      strcat(item->name, modifier);
     } else {
       size_t tokens = sscanf(line, "%s %u %u %u", item->name, &item->stats.cost,
        &item->stats.damage, &item->stats.armor);
      if (tokens != 4) {
        fprintf(stderr, "Only able to scan %zu in line: %s\n", tokens, line);
        exit(1);
      }
     }
    array_push(items, (intptr_t) item);
  }
  fclose(f);
}

void parse_boss(Entity *boss) {
  FILE *f = safe_fopen(BOSS_INPUT, "r");
  char line[MAX_LINE];
  fgets(line, MAX_LINE, f);
  sscanf(line, "Hit Points: %u", &boss->health);
  fgets(line, MAX_LINE, f);
  sscanf(line, "Damage: %u", &boss->stats.damage);
  fgets(line, MAX_LINE, f);
  sscanf(line, "Armor: %u", &boss->stats.armor);
  fclose(f);
}

void print_items(const char *category, Array *items) {
  printf("%s:\n", category);
  for (size_t i = 0; i < items->length; i++) {
    Item *item = (Item*) items->items[i];
    print_item(*item);
  }
  printf("\n");
}

void add_stats(Stats *dest, Stats src) {
  dest->cost += src.cost;
  dest->damage += src.damage;
  dest->armor += src.armor;
}

unsigned int real_damage(unsigned int damage, unsigned int armor) {
  int diff = damage - armor;
  return diff > 1 ? diff : 1;
}

unsigned int apply_damage(unsigned int health, unsigned int damage) {
  if (damage > health) {
    return 0;
  }
  return health - damage;
}

bool fight_won(Entity boss, Entity player) {
  while (player.health) {
    unsigned int player_damage = real_damage(player.stats.damage, boss.stats.armor);
    boss.health = apply_damage(boss.health, player_damage);
    if (!boss.health) {
      return true;
    }
    unsigned int boss_damage = real_damage(boss.stats.damage, player.stats.armor);
    player.health = apply_damage(player.health, boss_damage) ;
  }
  return false;
}

void print_entity(Entity e) {
  printf("health=%u damage=%u armor=%u\n", e.health, e.stats.damage,
    e.stats.armor);
}

unsigned int least_gold_to_win(Entity boss, Array *weapons, Array *armors,
  Array *rings)
{
  unsigned int gold = DEFAULT_GOLD;
  Array *best_items = NULL;
  for (size_t weapon_i = 0; weapon_i < weapons->length; weapon_i++) {
    for (int armor_i = -1; armor_i < (int) armors->length; armor_i++) {
      for (int ring_i = -1; ring_i < (int) rings->length; ring_i++) {
        for (int ring_j = -1; ring_j < (int) rings->length; ring_j++) {
          if (ring_i == ring_j) {
            continue;
          }
          Entity player = {0};
          player.health = 100;
          Array *item_set = array_new();
          array_push(item_set, weapons->items[weapon_i]);
          add_stats(&player.stats, ((Item*) weapons->items[weapon_i])->stats);
          if (armor_i != -1) {
            array_push(item_set, armors->items[armor_i]);
            add_stats(&player.stats, ((Item*) armors->items[armor_i])->stats);
          }
          if (ring_i != -1) {
            array_push(item_set, rings->items[ring_i]);
            add_stats(&player.stats, ((Item*) rings->items[ring_i])->stats);
          }
          if (ring_j != -1) {
            array_push(item_set, rings->items[ring_j]);
            add_stats(&player.stats, ((Item*) rings->items[ring_j])->stats);
          }
          if (!fight_won(boss, player) && player.stats.cost > gold) {
            gold = MAX(gold, player.stats.cost);
            if (best_items != NULL) {
              array_free(best_items, array_free_ignore);
            }
            best_items = item_set;
          } else {
            array_free(item_set, array_free_ignore);
          }
        }
      }
    }
  }
  print_items("Best item set", best_items);
  array_free(best_items, array_free_ignore);
  return gold;
}

int main() {
  Array *weapons = array_new();
  Array *armors = array_new();
  Array *rings = array_new();
  parse_shop(weapons, armors, rings);
  Entity boss = {0};
  parse_boss(&boss);
  unsigned int gold = least_gold_to_win(boss, weapons, armors, rings);
  printf("The most gold to lose is: %u\n", gold);
  array_free(weapons, NULL);
  array_free(armors, NULL);
  array_free(rings, NULL);
  return 0;
}
