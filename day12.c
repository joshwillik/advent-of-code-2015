#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include "deps/jsmn/jsmn.c"
#define PUZZLE_INPUT "day12.txt"
#define MAX_JSON_VALUE_LENGTH 256
#define SNIPPET_LENGTH 50
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define DEBUG 0
#define DEBUG_printf(...) if (DEBUG) { printf(__VA_ARGS__); }

char *read_input(char *file) {
  FILE *f = fopen(file, "r");
  size_t size = 1024, written = 0;
  char *data;
  data = realloc(NULL, size * sizeof(*data));
  int c;
  while ((c = fgetc(f)) != EOF){
    if (written == size - 1) {
      size *= 2;
      data = realloc(data, size);
    }
    if (c == '\n') {
      c = ' ';
    }
    data[written++] = c;
  };
  data[written] = '\0';
  fclose(f);
  return data;
}

int parse_tokens(char *input, jsmntok_t **tokens) {
  jsmn_parser parser;
  jsmn_init(&parser);
  int size = 1;
  *tokens = calloc(size, sizeof(jsmntok_t));
  jsmnerr_t err;
  do {
    err = jsmn_parse(&parser, input, *tokens, size);
    if (err == JSMN_ERROR_INVAL) {
      fprintf(stderr, "Invalid JSON token\n");
      exit(err);
    } else if (err == JSMN_ERROR_PART) {
      fprintf(stderr, "Incomplete JSON string\n");
      exit(err);
    } else if (err == JSMN_ERROR_NOMEM) {
      size *= 2;
      *tokens = realloc(*tokens, size * sizeof(jsmntok_t));
      if (!tokens) {
        fprintf(stderr, "Failed to allocate more memory: %i\n", size);
        exit(1);
      }
    }
  } while (err == JSMN_ERROR_NOMEM);
  return size;
}

void type_string(int type, char *str) {
  switch (type) {
    case JSMN_PRIMITIVE: sprintf(str, "primitive"); break;
    case JSMN_STRING: sprintf(str, "string"); break;
    case JSMN_OBJECT: sprintf(str, "object"); break;
    case JSMN_ARRAY: sprintf(str, "array"); break;
    default: sprintf(str, "unknown"); break;
  }
}

typedef struct jsmn_callback {
  void *result;
  void (*value_fn)(struct jsmn_callback *this, char *key, char *value);
  bool (*skip_fn)(char *json, jsmntok_t *tokens);
  void (*log_fn)(char *json, jsmntok_t tokens, char *key);
} jsmn_callback;

bool default_skip_fn(char *json, jsmntok_t *tokens) {
  (void)json;
  (void)tokens;
  return false;
}

void default_log_fn(char *json, jsmntok_t token, char *key) {
  (void)json;
  (void)token;
  (void)key;
}

jsmn_callback *jsmn_callback_new() {
  jsmn_callback *cb = malloc(sizeof(jsmn_callback));
  cb->result = calloc(1, sizeof(int));
  cb->skip_fn = default_skip_fn;
  cb->log_fn = default_log_fn;
  return cb;
}

void jsmn_callback_free(jsmn_callback *cb) {
  free(cb->result);
  free(cb);
}

void add_numbers(jsmn_callback *this, char *key, char *value) {
  (void)key; /* ignore */
  int *result = (int*) this->result;
  int v;
  if (sscanf(value, "%i", &v) == 1) {
    DEBUG_printf("(adding) %s: %s\n", key, value);
    *result += v;
  } else {
    DEBUG_printf("(ignoring) %s: %s\n", key, value);
  }
}

char *strslice(char *dest, char *src, size_t n) {
  while (n && *src) {
    char c = *src++;
    *dest++ = c;
    n--;
  }
  *dest = '\0';
  return dest;
}

void print_token(char *json, jsmntok_t token, char *key) {
  char type[16];
  type_string(token.type, type);
  char desc[SNIPPET_LENGTH+1];
  strslice(desc, json+token.start, MIN(token.end - token.start, SNIPPET_LENGTH));
  if (key) {
    printf("(token=%s) %s: %s {start: %d, end: %d, size: %d}\n",
      type, key, desc, token.start, token.end, token.size);
  } else {
    printf("(token=%s) %s {start: %d, end: %d, size: %d}\n",
      type, desc, token.start, token.end, token.size);
  }
}

void token_value(char *value, char *json, jsmntok_t token, int max) {
  strslice(value, json+token.start, MIN(token.end-token.start, max));
}

bool has_children(jsmntok_t t) {
  return t.type == JSMN_ARRAY || t.type == JSMN_OBJECT;
}

int jsmn_walk(char *json, jsmntok_t *tokens, jsmn_callback *cb, bool recursive,
  char *current_key)
{
  cb->log_fn(json, *tokens, current_key);
  int processed = 1;
  char str[MAX_JSON_VALUE_LENGTH+1];
  token_value(str, json, *tokens, MAX_JSON_VALUE_LENGTH);
  void (*value_fn)(jsmn_callback*, char*, char*) = cb->value_fn;
  if (cb->skip_fn(json, tokens)) {
    cb->value_fn = NULL;
  }
  char _key[MAX_JSON_VALUE_LENGTH];
  switch (tokens->type) {
    case JSMN_ARRAY: {
      if (!recursive) {
        cb->value_fn = NULL;
      }
      for (int i = 0; i < tokens->size; i++) {
        sprintf(_key, "%i", i);
        bool restore = false;
        if (!recursive && has_children(tokens[processed])) {
          cb->value_fn = NULL;
          restore = true;
        }
        processed += jsmn_walk(json, tokens + processed, cb, recursive, _key);
        if (restore) {
          cb->value_fn = value_fn;
        }
      }
    } break;
    case JSMN_OBJECT: {
      for (int i = 0; i < tokens->size; i += 2) {
        token_value(_key, json, tokens[processed++], MAX_JSON_VALUE_LENGTH);
        bool restore = false;
        if (!recursive && has_children(tokens[processed])) {
          cb->value_fn = NULL;
          restore = true;
        }
        processed += jsmn_walk(json, tokens + processed, cb, recursive, _key);
        if (restore) {
          cb->value_fn = value_fn;
        }
      }
    } break;
    case JSMN_STRING: {
      assert(current_key != NULL && current_key[0] != '\0');
      if (cb->value_fn) {
        cb->value_fn(cb, current_key, str);
      }
    } break;
    case JSMN_PRIMITIVE: {
      assert(current_key != NULL && current_key[0] != '\0');
      if (cb->value_fn) {
        cb->value_fn(cb, current_key, str);
        if (recursive) {
          DEBUG_printf("Skip=false %s\n", str);
        }
      } else if (recursive) {
        DEBUG_printf("Skip=true %s\n", str);
      }
    } break;
    default: {
      printf("Unhandled\n");
    } break;
  }
  cb->value_fn = value_fn;
  return processed;
}

void has_red(jsmn_callback *this, char *key, char *value) {
  (void)key; /*not needed*/
  if (!strcmp(value, "red")) {
    *((bool*) this->result) = true;
  }
}

bool skip_has_red(char *json, jsmntok_t *tokens) {
  if (tokens->type != JSMN_OBJECT) {
    return false;
  }
  jsmn_callback *cb = jsmn_callback_new();
  cb->value_fn = has_red;
  jsmn_walk(json, tokens, cb, false, NULL);
  bool has = *((bool*) cb->result);
  jsmn_callback_free(cb);
  return has;
}

int main() {
  char *json = read_input(PUZZLE_INPUT);
  jsmntok_t *tokens = NULL;
  parse_tokens(json, &tokens);
  jsmn_callback *cb = jsmn_callback_new();
  cb->value_fn = add_numbers;
  cb->skip_fn = skip_has_red;
  if (DEBUG) {
    cb->log_fn = print_token;
  }
  jsmn_walk(json, tokens, cb, true, NULL);
  printf("Final result: %u\n", *((int*) cb->result));
  jsmn_callback_free(cb);
  free(json);
  free(tokens);
}
