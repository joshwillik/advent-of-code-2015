CC = gcc
CFLAGS = -O2 -g -std=c11 -Wall -Wextra -Wpedantic -Werror -Wshadow -Wstrict-overflow -Wuninitialized -fno-strict-aliasing

all:
	mkdir -p bin

day1: day1.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day2: day2.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day3: day3.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day4: day4.c
	$(CC) $(CFLAGS) -lcrypto $? $(LDFLAGS) -o bin/$@

day5: day5.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day6: day6.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day7: day7.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day8: day8.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day9: day9.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day10: day10.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day11: day11.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day12: day12.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day13: day13.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day14: day14.c
	$(CC) -D _DEFAULT_SOURCE $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day15: day15.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day16: day16.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day17: day17.c
	$(CC) $(CFLAGS) -lm $? $(LDFLAGS) -o bin/$@

day18: day18.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day19: day19.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day20: day20.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day21: day21.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day22: day22.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day23: day23.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day24: day24.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@

day25: day25.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o bin/$@
