#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define PUZZLE_INPUT "day22.txt"
#define STARTING_HEALTH 50
#define STARTING_MANA 500
#define RECHARGE_MANA 101
#define NO_WINNING_PATH -1
#define MAX_SPELL_CHAIN 100

typedef struct Entity {
  unsigned int health;
  unsigned int damage;
  unsigned int mana;
  unsigned int shield;
  unsigned int shield_time;
  unsigned int poison;
  unsigned int poison_time;
  unsigned int recharge_time;
} Entity;

unsigned int real_damage(unsigned int raw_damage, unsigned int armor,
  unsigned int health)
{
  unsigned int damage = (armor >= raw_damage) ? 1 : (raw_damage - armor);
  return (health > damage) ? damage : health;
}

void apply_magic_missile(Entity *caster, Entity *target) {
  (void)caster;
  target->health -= real_damage(4, target->shield, target->health);
}

void apply_drain(Entity *caster, Entity *target) {
  caster->health += 2;
  target->health -= real_damage(2, target->shield, target->health);
}

bool can_cast_shield(Entity caster, Entity target) {
  (void)target;
  return !caster.shield_time;
}

void apply_shield(Entity *caster, Entity *target) {
  (void)target;
  caster->shield = 7;
  caster->shield_time = 6;
}

bool can_cast_poison(Entity caster, Entity target) {
  (void)caster;
  return !target.poison_time;
}

void apply_poison(Entity *caster, Entity *target) {
  (void)caster;
  target->poison = 3;
  target->poison_time = 6;
}

bool can_cast_recharge(Entity caster, Entity target) {
  (void)target;
  return !caster.recharge_time;
}

void apply_recharge(Entity *caster, Entity *target) {
  (void)target;
  caster->recharge_time = 5;
}

typedef struct Spell {
  char name[32];
  unsigned int cost;
  bool (*can_cast)(Entity caster, Entity target);
  void (*apply)(Entity *caster, Entity *target);
} Spell;

Spell spells[] = {
  {.name = "Magic Missile", .cost = 53, .can_cast = NULL, .apply = apply_magic_missile},
  {.name = "Drain", .cost = 73, .can_cast = NULL, .apply = apply_drain},
  {.name = "Shield", .cost = 113, .can_cast = can_cast_shield, .apply = apply_shield},
  {.name = "Poison", .cost = 173, .can_cast = can_cast_poison, .apply = apply_poison},
  {.name = "Recharge", .cost = 229, .can_cast = can_cast_recharge, .apply = apply_recharge},
};


void parse_boss(Entity *boss) {
  FILE *f = fopen(PUZZLE_INPUT, "r");
  if (!fscanf(f, "Hit Points: %u\n", &boss->health)) {
    fprintf(stderr, "Could not parse boss health\n");
    exit(1);
  }
  if (!fscanf(f, "Damage: %u\n", &boss->damage)) {
    fprintf(stderr, "Could not parse boss damage\n");
    exit(1);
  }
  fclose(f);
}

void effect_tick(Entity *entity) {
  if (entity->shield_time) {
    entity->shield_time--;
  } else {
    entity->shield = 0;
  }
  if (entity->recharge_time) {
    entity->recharge_time--;
    entity->mana += RECHARGE_MANA;
  }
  if (entity->poison_time) {
    entity->poison_time--;
    entity->health -= real_damage(entity->poison, entity->shield, entity->health);
  } else {
    entity->poison = 0;
  }
}

void print_state(Entity player, Entity boss, int level) {
  printf("Level %d:\n", level);
  printf("Player: health=%u mana=%u\n", player.health, player.mana);
  printf("Boss: health=%u\n\n", boss.health);
}

int find_least_cost_path(Entity player, Entity boss, bool hard_mode) {
  if (hard_mode) {
    player.health -= 1;
  }
  effect_tick(&player);
  effect_tick(&boss);
  if (!player.health) {
    return NO_WINNING_PATH;
  }
  if (!boss.health) {
    return 0;
  }
  int least_cost_path = NO_WINNING_PATH;
  for (size_t i = 0; i < sizeof(spells) / sizeof(spells[0]); i++) {
    Spell spell = spells[i];
    if (player.mana < spell.cost ||
      (spell.can_cast != NULL && !spell.can_cast(player, boss)))
    {
      continue;
    }
    Entity next_player = player;
    Entity next_boss = boss;
    next_player.mana -= spell.cost;
    spell.apply(&next_player, &next_boss);
    effect_tick(&next_player);
    effect_tick(&next_boss);
    if (!next_boss.health) {
      if (least_cost_path == NO_WINNING_PATH || spell.cost < (unsigned int) least_cost_path) {
        least_cost_path = spell.cost;
      }
      continue;
    }
    next_player.health -= real_damage(next_boss.damage, next_player.shield, next_player.health);
    if (!next_player.health) {
      continue;
    }
    int path_cost = find_least_cost_path(next_player, next_boss, hard_mode);
    if (path_cost == NO_WINNING_PATH) {
      continue;
    }
    path_cost += spell.cost;
    if (least_cost_path == NO_WINNING_PATH || path_cost < least_cost_path) {
      least_cost_path = path_cost;
    }
  }
  return least_cost_path;
}

int main() {
  Entity boss = {0};
  parse_boss(&boss);
  Entity player = {0};
  player.health = STARTING_HEALTH;
  player.mana = STARTING_MANA;
  int easy_least_mana = find_least_cost_path(player, boss, false);
  printf("Easy: least mana spent is: %d\n", easy_least_mana);
  int hard_least_mana = find_least_cost_path(player, boss, true);
  printf("Hard: least mana spent is: %d\n", hard_least_mana);
  return 0;
}
