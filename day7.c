#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "util/array.c"
#define PUZZLE_INPUT "day7.txt"
#define MAX_LINE 128
#define MAX_LABEL 16
#define DEBUG false

typedef enum wire_type {NOT, AND, OR, VALUE, RSHIFT, LSHIFT} wire_type;

struct Wire {
  char in1_label[3];
  struct Wire *in1;
  char in2_label[3];
  struct Wire *in2;
  wire_type type;
  uint16_t value;
  bool resolved;
  char label[20];
};

typedef struct Wire Wire;

Wire *wire_new() {
  Wire *wire = calloc(1, sizeof(Wire));
  return wire;
}

void print_wire(Wire *wire) {
  if (wire->type == RSHIFT) {
    printf("%s <- %s >> %d\n", wire->label, wire->in1_label, wire->value);
  } else if (wire->type == LSHIFT) {
    printf("%s <- %s << %d\n", wire->label, wire->in1_label, wire->value);
  } else if (wire->type == NOT) {
    printf("%s <- ^%s\n", wire->label, wire->in1_label);
  } else if (wire->type == VALUE && wire->in1) {
    printf("%s <- %s\n", wire->label, wire->in1->label);
  } else if (wire->type == VALUE) {
    printf("%s <- %d\n", wire->label, wire->value);
  } else if (wire->type == AND) {
    printf("%s <- %s && %s\n", wire->label, wire->in1->label, wire->in2->label);
  } else if (wire->type == OR) {
    printf("%s <- %s || %s\n", wire->label, wire->in1->label, wire->in2->label);
  } else {
    printf("Invalid wire\n");
  }
}

Wire* find_wire(Array *wires, char *label) {
  for (size_t i = 0; i < wires->length; i++) {
    Wire *wire = (Wire*) wires->items[i];
    if (!strcmp(wire->label, label)) {
      return wire;
    }
  }
  return NULL;
}

bool parse_NOT(Wire *wire, char *wire_str) {
  if (!strstr(wire_str, "NOT")) {
    return false;
  }
  wire->type = NOT;
  int scanned = sscanf(wire_str, "NOT %s -> %s", wire->in1_label, wire->label);
  if (scanned != 2) {
    fprintf(stderr, "Unable to parse NOT '%s'\n", wire_str);
  }
  return true;
}

bool parse_AND(Wire *wire, char *wire_str) {
  if (!strstr(wire_str, "AND")) {
    return false;
  }
  wire->type = AND;
  int scanned = sscanf(wire_str, "%s AND %s -> %s", wire->in1_label, wire->in2_label, wire->label);
  if (scanned != 3) {
    fprintf(stderr, "Unable to parse AND '%s'\n", wire_str);
  }
  return true;
}

bool parse_OR(Wire *wire, char *wire_str) {
  if (!strstr(wire_str, "OR")) {
    return false;
  }
  wire->type = OR;
  int scanned = sscanf(wire_str, "%s OR %s -> %s", wire->in1_label,
    wire->in2_label, wire->label);
  if (scanned != 3) {
    fprintf(stderr, "Unable to parse OR '%s'\n", wire_str);
  }
  return true;
}

bool parse_LSHIFT(Wire *wire, char *wire_str) {
  if (!strstr(wire_str, "LSHIFT")) {
    return false;
  }
  wire->type = LSHIFT;
  int scanned = sscanf(wire_str, "%s LSHIFT %s -> %s", wire->in1_label,
    wire->in2_label, wire->label);
  if (scanned != 3) {
    fprintf(stderr, "Unable to parse LSHIFT '%s'\n", wire_str);
  }
  return true;
}

bool parse_RSHIFT(Wire *wire, char *wire_str) {
  if (!strstr(wire_str, "RSHIFT")) {
    return false;
  }
  wire->type = RSHIFT;
  int scanned = sscanf(wire_str, "%s RSHIFT %s -> %s", wire->in1_label,
    wire->in2_label, wire->label);
  if (scanned != 3) {
    fprintf(stderr, "Unable to parse RSHIFT '%s'\n", wire_str);
  }
  return true;
}

bool parse_VALUE(Wire *wire, char *wire_str) {
  wire->type = VALUE;
  int scanned = sscanf(wire_str, "%s -> %s", wire->in1_label, wire->label);
  if (scanned != 2) {
    fprintf(stderr, "Unable to parse VALUE '%s'\n", wire_str);
  }
  return true;
}

Wire* parse_wire(char *wire_str) {
  Wire *wire = wire_new();
  bool success = parse_NOT(wire, wire_str) ||
    parse_AND(wire, wire_str) ||
    parse_OR(wire, wire_str) ||
    parse_LSHIFT(wire, wire_str) ||
    parse_RSHIFT(wire, wire_str) ||
    parse_VALUE(wire, wire_str);
  if (!success) {
    fprintf(stderr, "Failed to parse %s\n", wire_str);
    exit(1);
  }
  return wire;
}

void truncate_line(char *str) {
  int i = 0;
  while (str[++i] != '\n');
  str[i] = '\0';
}

Array* parse_wires() {
  Array *wires = array_new();
  FILE *f = fopen(PUZZLE_INPUT, "r");
  char line[MAX_LINE];
  while (fgets(line, MAX_LINE, f)) {
    truncate_line(line);
    Wire* wire = parse_wire(line);
    array_push(wires, (intptr_t) wire);
  }
  fclose(f);
  return wires;
}

Wire *pseudo_wire(uint16_t value) {
  Wire *wire = wire_new();
  wire->type = VALUE;
  wire->value = value;
  wire->resolved = true;
  sprintf(wire->label, "pseudo(%d)", wire->value);
  return wire;
}

void pseudo_value_wires(Array *wires) {
  for (size_t i = 0, max = wires->length; i < max; i++) {
    Wire *wire = (Wire*) wires->items[i];
    uint16_t value;
    if (*wire->in1_label && sscanf(wire->in1_label, "%hu", &value)) {
      Wire *value_wire = pseudo_wire(value);
      *wire->in1_label = '\0';
      wire->in1 = value_wire;
      array_push(wires, (intptr_t) value_wire);
    }
    if (*wire->in2_label && sscanf(wire->in2_label, "%hu", &value)) {
      Wire *value_wire = pseudo_wire(value);
      *wire->in2_label = '\0';
      wire->in2 = value_wire;
      array_push(wires, (intptr_t) value_wire);
    }
  }
}

void link_wires(Array *wires) {
  for (size_t i = 0; i < wires->length; i++) {
    Wire *wire = (Wire*) wires->items[i];
    if (*wire->in1_label) {
      wire->in1 = find_wire(wires, wire->in1_label);
    }
    if (*wire->in2_label) {
      wire->in2 = find_wire(wires, wire->in2_label);
    }
  }
}

int resolve_value(Wire *wire) {
  if (!wire->resolved) {
    if (DEBUG) {
      printf("%s: resolving\n", wire->label);
    }
    if (wire->type == VALUE) {
      wire->value = wire->in1 ? resolve_value(wire->in1) : wire->value;
    } else if (wire->type == NOT) {
      wire->value = ~resolve_value(wire->in1);
    } else if (wire->type == LSHIFT) {
      wire->value = resolve_value(wire->in1) << resolve_value(wire->in2);
    } else if (wire->type == RSHIFT) {
      wire->value = resolve_value(wire->in1) >> resolve_value(wire->in2);
    } else if (wire->type == AND) {
      wire->value = resolve_value(wire->in1) & resolve_value(wire->in2);
    } else if (wire->type == OR) {
      wire->value = resolve_value(wire->in1) | resolve_value(wire->in2);
    } else {
      fprintf(stderr, "Unable to resolve wire %s\n", wire->label);
      exit(1);
    }
    wire->resolved = true;
    if (DEBUG) {
      printf("Resolved %s: %d\n", wire->label, wire->value);
    }
  }
  return wire->value;
}

int main() {
  Array *wires = parse_wires();
  pseudo_value_wires(wires);
  link_wires(wires);
  Wire *start = find_wire(wires, "a");
  int32_t a_value = resolve_value(start);
  printf("%s #1 -> %d\n", start->label, a_value);
  // override B with the A output and re-run
  for (size_t i = 0; i < wires->length; i++) {
    ((Wire*) wires->items[i])->resolved = false;
  }
  Wire *b = find_wire(wires, "b");
  b->resolved = true;
  b->value = a_value;
  printf("%s #2 -> %d\n", start->label, resolve_value(start));
  array_free(wires, NULL);
}
