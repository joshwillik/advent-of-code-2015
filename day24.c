#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "util/array.c"
#define PUZZLE_INPUT "day24.txt"
bool debug_mode = false;

Array *parse_presents() {
  Array *presents = array_new();
  FILE *f = fopen(PUZZLE_INPUT, "r");
  int weight;
  while (fscanf(f, "%i", &weight) != EOF) {
    int *present = malloc(sizeof(int));
    *present = weight;
    array_push(presents, (intptr_t) present);
  }
  fclose(f);
  return presents;
}

int array_sum(Array *ints) {
  int sum = 0;
  for (size_t i = 0; i < ints->length; i++) {
    sum += *(int*) ints->items[i];
  }
  return sum;
}

int stack_sum(Array *presents, bool *indexes) {
  int sum = 0;
  for (size_t i = 0; i < presents->length; i++) {
    if (indexes[i]) {
      sum += *(int*) presents->items[i];
    }
  }
  return sum;
}

Array *snapshot_stack(Array *presents, bool *indexes) {
  Array *stack = array_new();
  for (size_t i = 0; i < presents->length; i++) {
    if (indexes[i]) {
      array_push(stack, presents->items[i]);
    }
  }
  return stack;
}

/* is_ended returns true when all the ON elements are at the end */
bool is_ended(bool *indexes, size_t size) {
  bool zero_found = false;
  for (size_t i = size; i--;) {
    if (!indexes[i]) {
      zero_found = true;
    } else if (zero_found) {
      return false;
    }
  }
  return true;
}

bool inc_combination(bool *indexes, size_t size) {
  if (is_ended(indexes, size)) {
    return false;
  }
  size_t last_i = size - 1;
  size_t i = size;
  /* walk back until we find an ON */
  while (i--) {
    if (indexes[i]) {
      break;
    }
  }
  if (i < last_i && !indexes[i + 1]) {
    indexes[i] = false;
    indexes[i+1] = true;
    return true;
  }
  unsigned int indexes_to_enable = 0;
  bool zero_found = false;
  do {
    if (indexes[i]) {
      indexes_to_enable++;
      indexes[i] = false;
      if (zero_found) {
        break;
      }
    } else {
      zero_found = true;
    }
  } while (i--);
  for (size_t j = 1; j <= indexes_to_enable; j++) {
    indexes[i+j] = true;
  }
  return true;
}

Array *all_stacks_of_size(Array *presents, int target_sum, size_t count) {
  Array *stacks = NULL;
  bool *indexes = calloc(presents->length, sizeof(bool));
  for (size_t i = 0; i < count; i++) {
    indexes[i] = true;
  }
  do {
    if (stack_sum(presents, indexes) == target_sum) {
      stacks = (stacks != NULL) ? stacks : array_new();
      array_push(stacks, (intptr_t) snapshot_stack(presents, indexes));
    }
  } while (inc_combination(indexes, presents->length));
  free(indexes);
  return stacks;
}

Array *least_count_valid_stacks(Array *presents, int target_sum) {
  for (size_t i = 1; i <= presents->length; i++) {
    Array *stacks = all_stacks_of_size(presents, target_sum, i);
    if (stacks != NULL) {
      return stacks;
    }
  }
  return NULL;
}

size_t quantum_entanglement(Array *stack) {
  size_t product = 1;
  for (size_t i = 0; i < stack->length; i++) {
    product *= *(size_t*) stack->items[i];
  }
  return product;
}

void print_stack(Array *stack) {
  printf("(QE=%zu) ", quantum_entanglement(stack));
  for (size_t j = 0; j < stack->length; j++) {
    printf("%d ", *(int*) stack->items[j]);
  }
  printf("\n");
}

Array *find_best_stack(Array *presents, int num_stacks){
  int stack_sum = array_sum(presents) / num_stacks;
  Array *stacks = least_count_valid_stacks(presents, stack_sum);
  Array *best_stack = (Array*) stacks->items[0];
  for (size_t i = 0; i < stacks->length; i++) {
    Array *stack = (Array*) stacks->items[i];
    if (debug_mode) {
      printf("valid stack: ");
      print_stack(stack);
    }
    if (quantum_entanglement(best_stack) > quantum_entanglement(stack)) {
      best_stack = stack;
    }
  }
  for (size_t i = 0; i < stacks->length; i++) {
    Array *current_stack = (Array*) stacks->items[i];
    if (current_stack != best_stack) {
      array_free(current_stack, array_free_ignore);
    }
  }
  array_free(stacks, array_free_ignore);
  return best_stack;
}

int main(int argc, char **argv) {
  for (size_t i = 0; i < (size_t)argc; i++) {
    if (!strcmp(argv[i], "-v")) {
      debug_mode = true;
      break;
    }
  }
  Array *presents = parse_presents();
  Array *best_stack_3 = find_best_stack(presents, 3);
  printf("Best stack 1/3: ");
  print_stack(best_stack_3);
  Array *best_stack_4 = find_best_stack(presents, 4);
  printf("Best stack 1/4: ");
  print_stack(best_stack_4);
  array_free(best_stack_3, array_free_ignore);
  array_free(best_stack_4, array_free_ignore);
  array_free(presents, NULL);
}
