#include <stdio.h>
#define _BSD_SOURCE
#include <features.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include "util/array.c"
#define PUZZLE_INPUT "day14.txt"
#define MAX_LINE 128
#define SECONDS 2503
#define INTERACTIVE false

typedef struct Reindeer {
  char name[16];
  int speed;
  int fly_seconds;
  int rest_seconds;
  bool is_flying;
  int fly_left;
  int rest_left;
  int distance;
  int points;
} Reindeer;

void parse_line(Array *reindeer, char *line) {
  Reindeer *r = malloc(sizeof(Reindeer));
  r->is_flying = true;
  r->distance = 0;
  r->points = 0;
  int n_tokens = sscanf(line,
    "%s can fly %d km/s for %d seconds, but then must rest for %d seconds.\n",
    r->name, &r->speed, &r->fly_seconds, &r->rest_seconds);
  if (n_tokens != 4) {
    fprintf(stderr, "Cannot parse line: %s\n", line);
    exit(1);
  }
  r->fly_left = r->fly_seconds;
  r->rest_left = r->rest_seconds;
  array_push(reindeer, (intptr_t) r);
}

void parse_input(Array *reindeer) {
  char line[MAX_LINE];
  FILE *f = fopen(PUZZLE_INPUT, "r");
  while (fgets(line, MAX_LINE, f)) {
    parse_line(reindeer, line);
  }
  fclose(f);
}

Reindeer *find_winner(Array *reindeer) {
  Reindeer *winner = (Reindeer*) reindeer->items[0];
  for (size_t i = 0; i < reindeer->length; i++) {
    Reindeer *r = (Reindeer*) reindeer->items[i];
    winner = (r->distance > winner->distance) ? r : winner;
  }
  return winner;
}

Reindeer *find_highest_points(Array *reindeer) {
  Reindeer *winner = (Reindeer*) reindeer->items[0];
  for (size_t i = 0; i < reindeer->length; i++) {
    Reindeer *r = (Reindeer*) reindeer->items[i];
    winner = (r->points > winner->points) ? r : winner;
  }
  return winner;
}

void apply_time(Array *reindeer) {
  for (size_t j = 0; j < reindeer->length; j++) {
    Reindeer *r = (Reindeer*) reindeer->items[j];
    if (r->is_flying) {
      r->distance += r->speed;
      r->fly_left--;
    } else {
      r->rest_left--;
    }
    if (!r->fly_left) {
      r->fly_left = r->fly_seconds;
      r->is_flying = false;
    }
    if (!r->rest_left) {
      r->rest_left = r->rest_seconds;
      r->is_flying = true;
    }
  }
  Reindeer *winner = find_winner(reindeer);
  winner->points++;
}

void clear() {
  printf("\033[2J\033[H");
}

void print_state(Array *reindeer, int time) {
  clear();
  printf("Second %d:\n", time);
  for (size_t j = 0; j < reindeer->length; j++) {
    Reindeer *r = (Reindeer*) reindeer->items[j];
    if (r->is_flying) {
      printf("%7s distance=%04dkm flying=%04ds points=%04d\n", r->name,
        r->distance, r->fly_left, r->points);
    } else {
      printf("%7s distance=%04dkm resting=%03ds points=%04d\n", r->name,
        r->distance, r->rest_left, r->points);
    }
  }
  Reindeer *winner = find_winner(reindeer);
  printf("Winner: %s\n", winner->name);
}

void wait() {
  struct timespec req, rem;
  req.tv_sec = 0;
  req.tv_nsec = 1000000;
  nanosleep(&req, &rem);
}

int main() {
  Array *reindeer = array_new();
  parse_input(reindeer);
  for (size_t i = 0; i < SECONDS; i++) {
    if (INTERACTIVE) {
      print_state(reindeer, i);
      wait();
    }
    apply_time(reindeer);
  }
  if (INTERACTIVE) {
    print_state(reindeer, 1000);
  }
  Reindeer *winner = find_highest_points(reindeer);
  printf("Winner is %s: %i km with %i points\n", winner->name, winner->distance,
    winner->points);
  array_free(reindeer, NULL);
  return 0;
}
