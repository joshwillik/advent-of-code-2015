#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "util/array.c"
#define PUZZLE_INPUT "day17.txt"
#define EGGNOG_SIZE 150

Array *parse_input() {
  Array *buckets = array_new();
  FILE *f = fopen(PUZZLE_INPUT, "r");
  char line[16];
  while (fgets(line, 16, f)) {
    int bucket;
    sscanf(line, "%d", &bucket);
    array_push(buckets, (intptr_t) bucket);
  }
  fclose(f);
  return buckets;
}

typedef struct State {
  unsigned long *n_bucket_count;
  unsigned long valid_combinations;
} State;

void _combinations(Array *buckets, size_t index, int total_eggnog,
  int total_buckets, int wanted_total, State *state)
{
  if (index == buckets->length) {
    int valid = (int) total_eggnog == wanted_total;
    state->n_bucket_count[total_buckets] += valid;
    state->valid_combinations += valid;
    return;
  }
  int bucket = (int) buckets->items[index];
  for (size_t i = 0; i <= 1; i++) {
    total_eggnog += i*bucket;
    _combinations(buckets, index + 1, total_eggnog, total_buckets + i, wanted_total,
      state);
  }
}

State combinations(Array *buckets, int wanted_total) {
  State state = {0};
  state.n_bucket_count = calloc(buckets->length, sizeof(unsigned long));
  _combinations(buckets, 0, 0, 0, wanted_total, &state);
  return state;
}

int main() {
  Array *buckets = parse_input();
  State result = combinations(buckets, EGGNOG_SIZE);
  printf("Valid combinations: %lu/%lu\n", result.valid_combinations,
    (unsigned long) pow(2, buckets->length));
  for (size_t i = 0; i < buckets->length; i++) {
    if (result.n_bucket_count[i]) {
      printf("Valid ways to use %lu buckets: %lu\n", i+1, result.n_bucket_count[i]);
      break;
    }
  }
  free(result.n_bucket_count);
  array_free(buckets, array_free_ignore);
}
