#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include "util/array.c"
#define PUZZLE_INPUT "day16.txt"
#define MAX_LINE 80
#define lengthof(a) (sizeof(a) / sizeof(a[0]))

typedef struct Property {
  char name[16];
  unsigned int value;
} Property;

Property properties[] = {
  {"children", 3},
  {"cats", 7},
  {"samoyeds", 2},
  {"pomeranians", 3},
  {"akitas", 0},
  {"vizslas", 0},
  {"goldfish", 5},
  {"trees", 3},
  {"cars", 2},
  {"perfumes", 1},
};

typedef struct Sue {
  int n;
  int matches;
  Property properties[3];
} Sue;

int intlen(int i) {
  assert(i >= 0);
  int len = 1, max = 10;
  while (i > max) {
    max *= 10;
    len++;
  }
  return len;
}

bool property_match(Property criterion, Property p) {
  if (strcmp(criterion.name, p.name)) {
    return false;
  }
  if (!strcmp(p.name, "trees") || !strcmp(p.name, "cats")) {
    return p.value > criterion.value;
  }
  if (!strcmp(p.name, "pomeranians") || !strcmp(p.name, "goldfish")) {
    return p.value < criterion.value;
  }
  return p.value == criterion.value;
}

int sue_matches(Sue *s) {
  int matches = 0;
  for (size_t i = 0, i_end = lengthof(properties); i < i_end; i++) {
    for (size_t j = 0, j_end = lengthof(s->properties); j < j_end; j++) {
      Property a = properties[i];
      Property b = s->properties[j];
      if (property_match(a, b)) {
        matches += 1;
      }
    }
  }
  return matches;
}

Sue *parse_sue(char *line) {
  Sue *sue = malloc(sizeof(Sue));
  sscanf(line, "Sue %d", &sue->n);
  line += 6 + intlen(sue->n); /* move to after "Sue N: " */
  int n_properties = 0;
  char name[30];
  int value;
  while (n_properties < 3) {
    sscanf(line, "%s %d", name, &value);
    line += 4 + (strlen(name) - 1) + intlen(value);
    int i = n_properties++;
    name[strlen(name) - 1] = '\0';
    strcpy(sue->properties[i].name, name);
    sue->properties[i].value = value;
  }
  sue->matches = sue_matches(sue);
  return sue;
}

Array *parse_input() {
  Array *sues = array_new();
  FILE *f = fopen(PUZZLE_INPUT, "r");
  char line[MAX_LINE];
  while (fgets(line, MAX_LINE, f)) {
    Sue *s = parse_sue(line);
    array_push(sues, (intptr_t) s);
  }
  fclose(f);
  return sues;
}

void print_sue(Sue *s) {
  printf("Sue %d:\n", s->n);
  for (size_t i = 0; i < 3; i++) {
    printf("%s: %d\n", s->properties[i].name, s->properties[i].value);
  }
  printf("\n");
}

int main() {
  Array *sues = parse_input();
  Sue *most = (Sue *) sues->items[0];
  for (size_t i = 0; i < sues->length; i++) {
    Sue *sue = (Sue *) sues->items[i];
    /* If this runs too slowly, compute matches inside sue at parse time */
    if (sue->matches > most->matches) {
      most = sue;
    }
  }
  printf("Sue %d matches most closely with %d matches\n", most->n, most->matches);
  array_free(sues, NULL);
  return 0;
}
