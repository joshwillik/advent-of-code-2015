#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#define PUZZLE_INPUT "hepxcrrq"

char forbidden[] = "iol";
bool is_forbidden(char c) {
  for (size_t i = 0; i < 3; i++) {
    if (forbidden[i] == c) {
      return true;
    }
  }
  return false;
}

void increment_password(char *password) {
  char *digit = password + strlen(password) - 1;
  int carry = 0;
  do {
    carry = *digit == 'z';
    if (!carry) {
      (*digit)++;
      carry = 0;
    } else {
      *digit = 'a';
      digit--;
    }
  } while (carry);
}

bool valid_password(char *password) {
  int max_run = 0, run = 0, set_ignore = 0, sets = 0;
  for (char *c = password, prev = '\0'; *c; prev = *c, c++) {
    if (is_forbidden(*c)) {
      return false;
    }
    if (!prev || *c == prev + 1) {
      run++;
      max_run = max_run > run ? max_run : run;
    } else {
      run = 1;
    }
    if (*c == prev && !set_ignore) {
      set_ignore = 1;
      sets++;
    } else if (set_ignore) {
      set_ignore--;
    }
  }
  return max_run >= 3 && sets >= 2;
}

bool should_break(char *password) {
  for (char *s = password; *s; s++) {
    if (*s != 'z') {
      return false;
    }
  }
  return true;
}

void find_next_password(char *password) {
  do {
    increment_password(password);
    if (should_break(password)) {
      break;
    }
  } while (!valid_password(password));
}

int main() {
  char password[9] = PUZZLE_INPUT;
  find_next_password(password);
  printf("Next password: %s\n", password);
  find_next_password(password);
  printf("Next password: %s\n", password);
  return 0;
}
