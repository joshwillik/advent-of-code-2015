#include <stdio.h>
#include <stdbool.h>
#define PUZZLE_INPUT "day18.txt"
#define SCREEN_X 100
#define SCREEN_Y 100
#define STEPS 100

void print_screen(bool screen[SCREEN_Y][SCREEN_X]) {
  for (size_t y = 0; y < SCREEN_Y; y++) {
    for (size_t x = 0; x < SCREEN_X; x++) {
      putchar(screen[y][x] ? '#' : '.');
    }
    putchar('\n');
  }
}

void apply_sticky_pixels(bool screen[SCREEN_Y][SCREEN_X]) {
  size_t last_x = SCREEN_X - 1;
  size_t last_y = SCREEN_Y - 1;
  for (size_t y = 0; y < SCREEN_Y; y++) {
    for (size_t x = 0; x < SCREEN_X; x++) {
      if ((x == 0 && y == 0) || (x == 0 && y == last_y) ||
        (x == last_x && y == 0) || (x == last_x && y == last_y))
      {
        screen[y][x] = true;
      }
    }
  }
}

void parse_screen(bool screen[SCREEN_Y][SCREEN_X]) {
  FILE *f = fopen(PUZZLE_INPUT, "r");
  char line[SCREEN_X + 2];
  int row = 0;
  while (fgets(line, SCREEN_X + 2, f)) {
    for (size_t i = 0; (bool) i < line[i]; i++) {
      screen[row][i] = line[i] == '#';
    }
    row++;
  }
  fclose(f);
  apply_sticky_pixels(screen);
}

unsigned int count_pixels(bool screen[SCREEN_Y][SCREEN_X]) {
  unsigned int n = 0;
  for (size_t y = 0; y < SCREEN_Y; y++) {
    for (size_t x = 0; x < SCREEN_X; x++) {
      n += (int) screen[y][x];
    }
  }
  return n;
}

unsigned int lit_neighbours(bool screen[SCREEN_Y][SCREEN_X], int y, int x) {
  unsigned int n = 0;
  int x_last = SCREEN_X - 1;
  int y_last = SCREEN_Y - 1;
  if (y > 0) {
    if (x > 0) {
      n += screen[y - 1][x - 1];
    }
    n += screen[y - 1][x];
    if (x < x_last) {
      n += screen[y - 1][x + 1];
    }
  }
  if (x > 0) {
    n += screen[y][x - 1];
  }
  if (x < x_last) {
    n += screen[y][x + 1];
  }
  if (y < y_last) {
    if (x > 0) {
      n += screen[y + 1][x - 1];
    }
    n += screen[y + 1][x];
    if (x < x_last) {
      n += screen[y + 1][x + 1];
    }
  }
  return n;
}

void apply_time(bool screen[SCREEN_Y][SCREEN_X]) {
  bool next[SCREEN_Y][SCREEN_X] = {0};
  for (size_t y = 0; y < SCREEN_Y; y++) {
    for (size_t x = 0; x < SCREEN_X; x++) {
      unsigned int lit = lit_neighbours(screen, y, x);
      if (screen[y][x]) {
        next[y][x] = lit == 3 || lit == 2;
      } else if (lit == 3) {
        next[y][x] = true;
      }
    }
  }
  for (size_t y = 0; y < SCREEN_Y; y++) {
    for (size_t x = 0; x < SCREEN_X; x++) {
      screen[y][x] = next[y][x];
    }
  }
  apply_sticky_pixels(screen);
}

int main() {
  bool screen[SCREEN_Y][SCREEN_X];
  parse_screen(screen);
  for (size_t i = 0; i < STEPS; i++) {
    apply_time(screen);
  }
  print_screen(screen);
  unsigned int n_lights = count_pixels(screen);
  printf("Lights on: %u\n", n_lights);
  return 0;
}
