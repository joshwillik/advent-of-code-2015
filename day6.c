#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "util/array.c"
#define PUZZLE_INPUT "day6.txt"
#define MAX_LINE 256

// TODO josh: Putting toggle in pixel_state is hacky.
// state should be bool, and command (enum pixel_state) should be (enum command)
typedef enum pixel_state {OFF, ON, TOGGLE} pixel_state;

typedef struct {
  int x;
  int y;
} Coords;

typedef struct {
  Coords from;
  Coords to;
  pixel_state state;
} Step;

intptr_t parse_step(char *step_str) {
  Step *step = calloc(1, sizeof(Step));
  char start[7] = {0};
  strncpy(start, step_str, 6);
  int from_y = 0, from_x = 0, to_y = 0, to_x = 0;
  if (!strcmp("toggle", start)) {
    int scanned = sscanf(step_str, "toggle %i,%i through %i,%i", &from_x, &from_y, &to_x, &to_y);
    if (scanned != 4) {
      fprintf(stderr, "Could not parse %s", step_str);
      exit(1);
    }
    step->from = (Coords) {from_x, from_y};
    step->to = (Coords) {to_x, to_y};
    step->state = TOGGLE;
  } else {
    char command[4] = {0};
    int scanned = sscanf(step_str, "turn %s %i,%i through %i,%i", command, &from_x, &from_y, &to_x, &to_y);
    if (scanned != 5) {
      fprintf(stderr, "Could not parse %s", step_str);
      exit(1);
    }
    step->from = (Coords) {from_x, from_y};
    step->to = (Coords) {to_x, to_y};
    step->state = !strcmp("on", command) ? ON : OFF;
  }
  return (intptr_t) step;
}

Array* parse_steps() {
  FILE *f = fopen(PUZZLE_INPUT, "r");
  Array *steps = array_new();
  char line[MAX_LINE];
  while (fgets(line, MAX_LINE, f)) {
    array_push(steps, parse_step(line));
  }
  fclose(f);
  return steps;
}

void apply_command(pixel_state *screen, Step *step) {
  for (int y = step->from.y; y <= step->to.y; y++) {
    for (int x = step->from.x; x <= step->to.x; x++) {
      int pixel = y * 1000 + x;
      if (step->state == TOGGLE) {
        screen[pixel] = screen[pixel] == ON ? OFF : ON;
      } else {
        screen[pixel] = step->state;
      }
    }
  }
}

void apply_command2(int *screen, Step *step) {
  for (int y = step->from.y; y <= step->to.y; y++) {
    for (int x = step->from.x; x <= step->to.x; x++) {
      int pixel = y * 1000 + x;
      if (step->state == OFF && screen[pixel]) {
        screen[pixel] -= 1;
      }
      if (step->state == ON) {
        screen[pixel] += 1;
      }
      if (step->state == TOGGLE) {
        screen[pixel] += 2;
      }
    }
  }
}

void print_step(Step *step) {
  char *name = step->state == TOGGLE ? "toggle" :
    step->state == ON ? "turn on" : "turn off";
  printf("%s (%d,%d) -> (%d, %d)\n", name, step->from.x, step->from.y,
    step->to.x, step->to.y);
}

int main () {
  Array *steps = parse_steps();
  pixel_state *screen1 = calloc(1000 * 1000, sizeof(pixel_state));
  for (size_t i = 0; i < steps->length; i++) {
    Step *step = (Step*) steps->items[i];
    apply_command(screen1, step);
  }
  int *screen2 = calloc(1000 * 1000, sizeof(int));
  for (size_t i = 0; i < steps->length; i++) {
    Step *step = (Step*) steps->items[i];
    apply_command2(screen2, step);
  }
  array_free(steps, NULL);
  int num_on = 0;
  for (size_t y = 0; y < 1000; y++) {
    for (size_t x = 0; x < 1000; x++) {
      if (((pixel_state*) screen1)[y * 1000 + x] == ON) {
        num_on += 1;
      }
    }
  }
  free((void*) screen1);
  int total_brightness = 0;
  for (size_t y = 0; y < 1000; y++) {
    for (size_t x = 0; x < 1000; x++) {
      total_brightness += ((int*) screen2)[y * 1000 + x];
    }
  }
  free((void*) screen2);
  printf("%d pixels are on for screen 1\n", num_on);
  printf("%d total brightness for screen 2\n", total_brightness);
  return 0;
}
