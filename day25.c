#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define PUZZLE_INPUT "day25.txt"
#define MAX_LINE 128
#define STARTING_VALUE 20151125

typedef struct coordinates {
  long unsigned x;
  long unsigned y;
} coordinates;

bool eat_word(char *word, char **line, size_t max_word) {
  while (**line == ' ' || **line == '\t') {
    (*line)++;
  }
  bool found = false;
  while (max_word--) {
    char c = **line;
    if (c == ' ' || c == '\t' || c == '\0') {
      break;
    }
    *word = c;
    word++;
    (*line)++;
    found = true;
  }
  *word = '\0';
  return found;
}

coordinates parse_input() {
  coordinates coords = {0};
  char *line = calloc(MAX_LINE, sizeof(char));
  char *orig_line = line;
  FILE *f = fopen(PUZZLE_INPUT, "r");
  fgets(line, MAX_LINE, f);
  char word[32] = {0};
  bool is_column = false, is_row = false;
  while (eat_word(word, &line, 31)) {
    if (is_row) {
      sscanf(word, "%lu", &coords.y);
      is_row = false;
    }
    if (is_column) {
      sscanf(word, "%lu", &coords.x);
      is_column = false;
    }
    if (!strcmp(word, "column")) {
      is_column = true;
    }
    if (!strcmp(word, "row")) {
      is_row = true;
    }
  }
  free(orig_line);
  fclose(f);
  return coords;
}

long unsigned find_series_index(long unsigned row, long unsigned column) {
  long unsigned prev_row = (row - 1) + (column - 1);
  return (prev_row * (prev_row + 1) / 2) + column;
}

long unsigned compute_series_value(long unsigned index) {
  long unsigned value = STARTING_VALUE;
  for (size_t i = 2; i <= index; i++) {
    value = (value * 252533) % 33554393;
  }
  return value;
}

int main() {
  coordinates coords = parse_input();
  long unsigned index = find_series_index(coords.y, coords.x);
  long unsigned value = compute_series_value(index);
  printf("Instruction y=%lu x=%lu n=%lu: %lu\n", coords.y, coords.x, index, value);
  return 0;
}
