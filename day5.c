#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#define PUZZLE_INPUT "day5.txt"
#define MAX_LINE_LENGTH 32

char forbidden[4][3] = {"ab", "cd", "pq", "xy"};
bool has_forbidden(char *str) {
  for (size_t i = 0; i < 4; i++) {
    if (strstr(str, forbidden[i])) {
      return true;
    }
  }
  return false;
}

bool has_duplicates(char *str) {
  for (size_t i = 0; i < strlen(str) - 1; i++) {
    if (str[i] == str[i+1]) {
      return true;
    }
  }
  return false;
}

char alphabet[] = "abcdefghijklmnopqrstuvwxyz";
// pattern /xx.*xx/
bool has_double_duplicate(char *str) {
  for (size_t i = 0; i < strlen(alphabet); i++) {
    for (size_t j = 0; j < strlen(alphabet); j++) {
      char pattern[3] = {alphabet[i], alphabet[j], '\0'};
      char *first = strstr(str, pattern);
      if (!first) {
        continue;
      }
      if (strstr(first+2, pattern)) {
        return true;
      }
    }
  }
  return false;
}

// pattern /x.x/
bool has_skip_duplicate(char *str) {
  for (size_t i = 0; i < strlen(str) - 2; i++) {
    if (str[i] == str[i + 2]) {
      return true;
    }
  }
  return false;
}

char vowels[] = "aeiou";
bool has_enough_vowels(char *str) {
  int count = 0;
  for (size_t i = 0; i < strlen(str); i++) {
    for (size_t j = 0; j < strlen(vowels); j++) {
      if (str[i] == vowels[j]) {
        count += 1;
        break;
      }
    }
  }
  return count >= 3;
}

bool is_nice(char *str) {
  return !has_forbidden(str) && has_duplicates(str) && has_enough_vowels(str);
}

bool is_nice2(char *str) {
  return has_double_duplicate(str) && has_skip_duplicate(str);
}

int main() {
  FILE *f = fopen(PUZZLE_INPUT, "r");
  char line[MAX_LINE_LENGTH] = {0};
  int nice = 0;
  int nice2 = 0;
  while (fgets(line, MAX_LINE_LENGTH, f)) {
    line[strlen(line) - 1] = '\0'; // drop the \n at the end of the line
    if (is_nice(line)) {
      nice += 1;
    }
    if (is_nice2(line)) {
      nice2 += 1;
    }
  }
  fclose(f);
  printf("%i nice lines\n", nice);
  printf("%i nice2 lines\n", nice2);
  return 0;
}
