#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include "util/array.c"
#define PUZZLE_INPUT "day9.txt"
#define MAX_LINE 256
#define DEBUG 0

typedef struct City {
  char name[50];
  Array *routes;
} City;

typedef struct Route {
  City *city;
  unsigned int distance;
} Route;

void print_city(City *city) {
  printf("%s\n----------\n", city->name);
  for (size_t i = 0; i < city->routes->length; i++) {
    Route *route = (Route*) city->routes->items[i];
    printf("%d to %s\n", route->distance, route->city->name);
  }
  printf("\n");
}

City *city_new(char *name) {
  City *c = (City*) calloc(1, sizeof(City));
  strcpy(c->name, name);
  c->routes = array_new();
  return c;
}

void city_free(void *_c) {
  City *c = _c;
  array_free(c->routes, NULL);
  free(_c);
}

City *find_or_create_city(Array *cities, char *name) {
  City *c;
  for (size_t i = 0; i < cities->length; i++) {
    c = (City*) cities->items[i];
    if (!strcmp(c->name, name)) {
      return c;
    }
  }
  c = city_new(name);
  array_push(cities, (intptr_t) c);
  return c;
}

void add_city_route(City *origin, unsigned int distance, City *destination) {
  Route *route = calloc(1, sizeof(Route));
  route->distance = distance;
  route->city = destination;
  array_push(origin->routes, (intptr_t) route);
}

void parse_city(char *line, Array *cities) {
  char origin[100];
  char destination[100];
  unsigned int distance;
  sscanf(line, "%s to %s = %ud", origin, destination, &distance);
  City *origin_city = find_or_create_city(cities, origin);
  City *destination_city = find_or_create_city(cities, destination);
  add_city_route(origin_city, distance, destination_city);
  add_city_route(destination_city, distance, origin_city);
}

Array *parse_cities() {
  Array *cities = array_new();
  FILE *f = fopen(PUZZLE_INPUT, "r");
  if (!f) {
    fprintf(stderr, "Could not open %s\n", PUZZLE_INPUT);
    exit(1);
  }
  char line[MAX_LINE];
  while (fgets(line, MAX_LINE, f)) {
    parse_city(line, cities);
  }
  fclose(f);
  return cities;
}

bool visited(Array *cities, City *city) {
  for (size_t i = 0; i < cities->length; i++) {
    City *c = (City*) cities->items[i];
    if (!strcmp(c->name, city->name)) {
      return true;
    }
  }
  return false;
}

Array *array_copy(Array *source) {
  Array *a = array_new();
  if (source) {
    for (size_t i = 0; i < source->length; i++) {
      array_push(a, source->items[i]);
    }
  }
  return a;
}

unsigned int best_path_from(City *start, Array *trip,
  unsigned int (*choose)(unsigned int a, unsigned int b))
{
  unsigned int best_trip = 0;
  for (size_t i = 0; i < start->routes->length; i++) {
    Route *route = (Route*) start->routes->items[i];
    Array *_trip = array_copy(trip);
    array_push(_trip, (intptr_t) start);
    if (!visited(_trip, route->city)) {
      unsigned int trip_total = route->distance +
        best_path_from(route->city, _trip, choose);
      if (DEBUG) {
        printf("Trip %d\n", trip_total);
      }
      best_trip = (*choose)(trip_total, best_trip);
    }
    free(_trip->items);
    free(_trip);
  }
  return best_trip;
}

unsigned int choose_shortest(unsigned int a, unsigned int b) {
  return ((a < b || b == 0) && a != 0) ? a : b;
}

unsigned int choose_longest(unsigned int a, unsigned int b) {
  return (a > b) ? a : b;
}

int main() {
  Array *cities = parse_cities();
  // for (size_t i = 0; i < cities->length; i++) {
  //   City *c = (City*) cities->items[i];
  //   print_city(c);
  // }
  int global_shortest = UINT_MAX;
  int global_longest = 0;
  for (size_t i = 0; i < cities->length; i++) {
    City *c = (City*) cities->items[i];
    unsigned int shortest = best_path_from(c, NULL, choose_shortest);
    global_shortest = choose_shortest(shortest, global_shortest);
    unsigned int longest = best_path_from(c, NULL, choose_longest);
    global_longest = choose_longest(longest, global_longest);
    if (DEBUG) {
      printf("Shortest path from %s = %d\n", c->name, shortest);
      printf("Longest path from %s = %d\n", c->name, longest);
    }
  }
  printf("Shortest path is: %i\n", global_shortest);
  printf("Longest path is: %i\n", global_longest);
  array_free(cities, city_free);
  return 0;
}
