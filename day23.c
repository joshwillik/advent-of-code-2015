#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "util/array.c"
#define PUZZLE_INPUT "day23.txt"
#define MAX_LINE 30

bool is_debug = false;

typedef enum reg {
  a,
  b,
} reg;

typedef enum instruction_type {
  hlf,
  tpl,
  inc,
  jmp,
  jie,
  jio,
} instruction_type;

typedef struct Instruction {
  instruction_type type;
  reg reg;
  int argument;
} Instruction;

void parse_instruction(Instruction *i, const char *line) {
  char type[4];
  strncpy(type, line, 3);
  if (!strcmp(type, "hlf")) {
    i->type = hlf;
  } else if (!strcmp(type, "tpl")) {
    i->type = tpl;
  } else if (!strcmp(type, "inc")) {
    i->type = inc;
  } else if (!strcmp(type, "jmp")) {
    i->type = jmp;
  } else if (!strcmp(type, "jie")) {
    i->type = jie;
  } else if (!strcmp(type, "jio")) {
    i->type = jio;
  } else {
    fprintf(stderr, "Could not parse instruction: %s (type)", line);
    exit(1);
  }
  char *args = (char*) line + 4;
  if (i->type == jmp) {
    if (sscanf(args, "%d", &i->argument) != 1) {
      fprintf(stderr, "Could not parse instruction: %s (arg)", line);
      exit(1);
    }
  } else {
    char str_reg[2];
    if (!sscanf(args, "%s %d", str_reg, &i->argument)) {
      fprintf(stderr, "Could not parse instruction: %s (reg,arg)", line);
      exit(1);
    }
    if (str_reg[strlen(str_reg) - 1] == ',') {
      str_reg[strlen(str_reg) - 1] = '\0';
    }
    if (!strcmp("a", str_reg)) {
      i->reg = a;
    } else if (!strcmp("b", str_reg)) {
      i->reg = b;
    } else {
      fprintf(stderr, "Could not parse instruction: %s %s (reg)", line, str_reg);
      exit(1);
    }
  }
}

Array *parse_input() {
  FILE *f = fopen(PUZZLE_INPUT, "r");
  Array *instructions = array_new();
  char line[MAX_LINE];
  while (fgets(line, MAX_LINE, f)) {
    Instruction *i = malloc(sizeof(Instruction));
    parse_instruction(i, line);
    array_push(instructions, (intptr_t) i);
  }
  fclose(f);
  return instructions;
}

void print_instruction(Instruction i) {
  if (i.type == hlf) {
    printf("hlf");
  } else if (i.type == tpl) {
    printf("tpl");
  } else if (i.type == inc) {
    printf("inc");
  } else if (i.type == jmp) {
    printf("jmp");
  } else if (i.type == jie) {
    printf("jie");
  } else if (i.type == jio) {
    printf("jio");
  } else {
    printf("unknown");
  }
  printf(": ");
  if (i.type == jmp) {
    printf("%d", i.argument);
  } else {
    if (i.reg == a) {
      printf("register(a)");
    } else if (i.reg == b) {
      printf("register(b)");
    } else {
      printf("register(unknown)");
    }
  }
  if (i.type == jie || i.type == jio) {
    printf(", %d", i.argument);
  }
  printf("\n");
}

void process_instructions(unsigned int *register_a, unsigned int *register_b,
  Array *instructions)
{
  size_t op_i = 0;
  while (op_i < instructions->length) {
    Instruction op = *(Instruction*) instructions->items[op_i];
    unsigned int *op_register = (op.reg == a) ? register_a : register_b;
    switch (op.type) {
      case inc:
        (*op_register)++;
        op_i++;
      break;
      case hlf:
        (*op_register) /= 2;
        op_i++;
      break;
      case tpl:
        (*op_register) *= 3;
        op_i++;
      break;
      case jmp:
        op_i += op.argument;
      break;
      case jie:
        if (*op_register % 2 == 0) {
          op_i += op.argument;
        } else {
          op_i++;
        }
      break;
      case jio:
        if (*op_register == 1) {
          op_i += op.argument;
        } else {
          op_i++;
        }
      break;
      default:
        fprintf(stderr, "Unrecognized instruction: #%zu\n", op_i);
        exit(1);
      break;
    }
    if (is_debug) {
      print_instruction(op);
      printf("a=%05u b=%05u\n\n", *register_a, *register_b);
    }
  }
}

int main(int argc, char **argv) {
  for (int i = 0; i < argc; i++) {
    if (!strcmp(argv[i], "-v")) {
      is_debug = true;
    }
  }
  Array *instructions = parse_input();
  if (is_debug) {
    for (size_t i = 0; i < instructions->length; i++) {
      Instruction *instruction = (Instruction*) instructions->items[i];
      print_instruction(*instruction);
    }
  }
  unsigned int register_a = 0, register_b = 0;
  process_instructions(&register_a, &register_b, instructions);
  printf("Part 1: register b: %u\n", register_b);
  register_a = 1;
  register_b = 0;
  process_instructions(&register_a, &register_b, instructions);
  printf("Part 2: register b: %u\n", register_b);
  array_free(instructions, NULL);
}
