#include <stdio.h>
#include <stdint.h>
#define puzzle_input "day1.txt"

int main() {
  FILE *input = fopen(puzzle_input, "r");
  if (!input) {
    puts(puzzle_input " does not exist");
    return 1;
  }
  char c;
  int32_t floor = 0;
  int32_t basement_char = 0;
  uint32_t counter = 0;
  while ((c = fgetc(input)) && c != EOF) {
    counter += 1;
    if (c == '(') {
      floor += 1;
    } else if (c == ')') {
      floor -= 1;
    }

    if (floor == -1 && !basement_char) {
      basement_char = counter;
    }
  }
  fclose(input);
  printf("Floor %d\n", floor);
  printf("Basement %d\n", basement_char);
  return 0;
}
