#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#define PUZZLE_INPUT "day2.txt"
#define INPUT_SIZE 1000
#define MAX_LINE 128

typedef struct {
  int a;
  int b;
  int c;
} Box;

int max(int a, int b) {
  return a > b ? a : b;
}

int max_3(int a, int b, int c) {
  return max(max(a, b), c);
}

void smallest_face(Box box, int *face) {
  if (max_3(box.a, box.b, box.c) == box.a) {
    face[0] = box.b;
    face[1] = box.c;
  } else if (max_3(box.a, box.b, box.c) == box.b) {
    face[0] = box.a;
    face[1] = box.c;
  } else {
    face[0] = box.a;
    face[1] = box.b;
  }
}

Box parse_box(const char *line) {
  Box box = {0};
  sscanf(line, "%dx%dx%d", &box.a, &box.b, &box.c);
  return box;
}

int main() {
  FILE *input = fopen(PUZZLE_INPUT, "r");
  if (!input) {
    puts(PUZZLE_INPUT " does not exist");
    return 1;
  }
  Box boxes[INPUT_SIZE];
  char line[MAX_LINE] = {0};
  int counter = 0;
  while(fgets(line, MAX_LINE, input)) {
    boxes[counter++] = parse_box(line);
  }
  unsigned long int paper = 0;
  unsigned long int ribbon = 0;
  for (int i = 0; i < counter; i++) {
    Box box = boxes[i];
    printf("Box %6d: a=%d b=%d c=%d\n", i+1, box.a, box.b, box.c);
    int face[2] = {0};
    smallest_face(box, face);
    paper += 2 * box.a * box.b + 2 * box.a * box.c + 2 * box.b * box.c + face[0] * face[1];
    ribbon += 2*face[0] + 2*face[1] + box.a * box.b * box.c;
  }
  printf("Total: paper=%lu ribbon=%lu\n", paper, ribbon);
  fclose(input);
  return 0;
}
