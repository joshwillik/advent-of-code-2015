#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "util/array.c"
#define PUZZLE_INPUT "day13.txt"
#define MAX_LINE 128
#define MAX(a, b) (((a) < (b)) ? (b) : (a))
#define DEBUG 0

typedef struct Neighbour {
  struct Person *person;
  struct Person *neighbour;
  int happiness;
} Neighbour;

struct Person {
  char name[16];
  Array *neighbours;
};

typedef struct Person Person;

int read_line(char *line, Person *src, Person *dest, Neighbour *n) {
  char type[5];
  int tokens = sscanf(line, "%s would %s %d happiness units by sitting next to %s",
    src->name, type, &n->happiness, dest->name);
  if (tokens != 4) {
    return tokens;
  }
  if (!strcmp(type, "lose")) {
    n->happiness = 0 - n->happiness;
  }
  int l = strlen(dest->name);
  if (dest->name[l-1] == '.') {
    dest->name[l-1] = '\0';
  }
  return 0;
}

Person *find_person(Array *people, Person person) {
  for (size_t i = 0; i < people->length; i++) {
    Person *saved_person = (Person*) people->items[i];
    if (!strcmp(person.name, saved_person->name)) {
      return saved_person;
    }
  }
  return NULL;
}

Neighbour *find_neighbour(Array *neighbours, Neighbour neighbour) {
  for (size_t i = 0; i < neighbours->length; i++) {
    Neighbour *saved_neighbour = (Neighbour*) neighbours->items[i];
    if (saved_neighbour->person == neighbour.person &&
      saved_neighbour->neighbour == neighbour.neighbour)
    {
      return saved_neighbour;
    }
  }
  return NULL;
}

Person *person_new() {
  Person *p = malloc(sizeof(Person));
  p->neighbours = array_new();
  return p;
}

void person_free(void *p) {
  Person *person = p;
  array_free(person->neighbours, array_free_ignore);
  free(p);
}

Person *add_person(Array *people, Person person) {
  Person *new_person = person_new();
  strcpy(new_person->name, person.name);
  array_push(people, (intptr_t) new_person);
  return new_person;
}

Neighbour *add_neighbour(Array *neighbours, Neighbour neighbour){
  Neighbour *new_neighbour = malloc(sizeof(Neighbour));
  new_neighbour->happiness = neighbour.happiness;
  new_neighbour->person = neighbour.person;
  new_neighbour->neighbour = neighbour.neighbour;
  array_push(neighbours, (intptr_t) new_neighbour);
  return new_neighbour;
}

void print_person(Person person) {
  printf("(Person) %s\n", person.name);
}

void print_neighbour(Neighbour n) {
  printf("(Neighbour) %s gets %d happiness from sitting next to %s\n", n.person->name,
    n.happiness, n.neighbour->name);
}

void parse_input(Array *people, Array *neighbours) {
  FILE *f = fopen(PUZZLE_INPUT, "r");
  char line[MAX_LINE];
  while (fgets(line, MAX_LINE, f)) {
    Person _src, _dest;
    Neighbour _neighbour;
    int parse_status = read_line(line, &_src, &_dest, &_neighbour);
    if (parse_status) {
      fprintf(stderr, "Could not parse line, %d tokens: %s\n", parse_status,
        line);
      exit(1);
    }
    Person *src = find_person(people, _src);
    if (!src) {
      src = add_person(people, _src);
    }
    Person *dest = find_person(people, _dest);
    if (!dest) {
      dest = add_person(people, _dest);
    }
    _neighbour.person = src;
    _neighbour.neighbour = dest;
    Neighbour *neighbour  = find_neighbour(neighbours, _neighbour);
    if (!neighbour) {
      neighbour = add_neighbour(neighbours, _neighbour);
    }
    array_push(src->neighbours, (intptr_t) neighbour);
  }
  fclose(f);
}

void add_self(Array *people, Array *neighbours) {
  Person *me = person_new();
  strcpy(me->name, "Me");
  size_t n_others = people->length;
  array_push(people, (intptr_t) me);
  for (size_t i = 0; i < n_others; i++) {
    Person *other = (Person*) people->items[i];
    Neighbour n;
    n.happiness = 0;
    n.person = me;
    n.neighbour = other;
    Neighbour *neighbour = add_neighbour(neighbours, n);
    array_push(me->neighbours, (intptr_t) neighbour);
    n.neighbour = me;
    n.person = other;
    neighbour = add_neighbour(neighbours, n);
    array_push(other->neighbours, (intptr_t) neighbour);
  }
}


Neighbour *persons_neighbour(Array *neighbours, Person *wanted_neighbour) {
  for (size_t i = 0; i < neighbours->length; i++) {
    Neighbour *neighbour = (Neighbour*) neighbours->items[i];
    if (neighbour->neighbour == wanted_neighbour) {
      return neighbour;
    }
  }
  return NULL;
}

int happiness_pairing(Person *a, Person *b) {
  int a_happy = persons_neighbour(a->neighbours, b)->happiness;
  int b_happy = persons_neighbour(b->neighbours, a)->happiness;
  if (DEBUG) {
    printf("%s gets %i happiness from %s\n", a->name, a_happy, b->name);
    printf("%s gets %i happiness from %s\n", b->name, b_happy, a->name);
  }
  return a_happy + b_happy;
}

int table_happiness(Array *group) {
  int happiness = 0;
  for (size_t i = 0; i < group->length; i++) {
    Person *person = (Person*) group->items[i];
    Person *next = (Person*) group->items[(i + 1) % group->length];
    happiness += happiness_pairing(person, next);
  }
  return happiness;
}

void circular_permutation(Array *results, Array *prefix, size_t start) {
  if (start == prefix->length) {
    array_push(results, (intptr_t) array_clone(prefix));
  }
  for (size_t i = start; i < prefix->length; i++) {
    array_swap(prefix, start, i);
    circular_permutation(results, prefix, start + 1);
    array_swap(prefix, start, i);
  }
}

Array *table_groups(Array *people) {
  Array *groups = array_new();
  circular_permutation(groups, people, 0);
  return groups;
}

void group_free(void *group) {
  array_free((Array*) group, array_free_ignore);
}

void print_group(Array *group) {
  (void)group;
}

int main() {
  Array *people = array_new();
  Array *neighbours = array_new();
  parse_input(people, neighbours);
  add_self(people, neighbours);
  Array *groups = table_groups(people);
  int happiness = 0;
  for (size_t i = 0; i < groups->length; i++) {
    Array *group = (Array*) groups->items[i];
    if (DEBUG) {
      printf("Group %lu:\n", i);
    }
    int local_happiness = table_happiness(group);
    if (DEBUG) {
      printf("Total happiness: %i\n\n", local_happiness);
    }
    happiness = MAX(happiness, local_happiness);
  }
  printf("Best arrangement happiness in %lu groups: %i\n", groups->length, happiness);
  array_free(groups, group_free);
  array_free(people, person_free);
  array_free(neighbours, NULL);
  return 0;
}
