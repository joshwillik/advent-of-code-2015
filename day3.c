#include <stdio.h>
#include <stdlib.h>
#define PUZZLE_INPUT "day3.txt"

typedef enum {
  up,
  right,
  down,
  left,
} direction;

typedef struct {
  int length;
  void *items;
} Array;

typedef struct {
  int x;
  int y;
} position;

void array_free(Array array) {
  free(array.items);
}

Array parse_steps() {
  FILE *input = fopen(PUZZLE_INPUT, "r");
  if (!input) {
    puts(PUZZLE_INPUT " does not exist");
    exit(1);
  }
  fseek(input, 0, SEEK_END);
  int size = ftell(input);
  fseek(input, 0, SEEK_SET);
  Array steps;
  steps.length = 0;
  steps.items = calloc(size, sizeof(direction));
  char c;
  while ((c = fgetc(input)) && c != EOF) {
    direction step;
    switch (c) {
      case '^': step = up; break;
      case '>': step = right; break;
      case 'v': step = down; break;
      case '<': step = left; break;
      case '\n': continue; break;
      default:
        printf("Error %i\n", c);
        exit(1);
        break;
    }
    ((direction*)steps.items)[steps.length++] = step;
  }
  fclose(input);
  return steps;
}

int count_visits(Array positions) {
  int max_x = 0, max_y = 0, min_x = 0, min_y = 0;
  for (int i = 0; i < positions.length; i++) {
    position p = ((position*) positions.items)[i];
    if (p.x < min_x) {
      min_x = p.x;
    }
    if (p.x > max_x) {
      max_x = p.x;
    }
    if (p.y < min_y) {
      min_y = p.y;
    }
    if (p.y > max_y) {
      max_y = p.y;
    }
  }
  int x_range = max_x - min_x + 1;
  int y_range = max_y - min_y + 1;
  int num_positions = y_range * x_range;
  int *times_visited = calloc(num_positions, sizeof(int));
  for (int i = 0; i < positions.length; i++) {
    position p = ((position*) positions.items)[i];
    int _x = p.x - min_x;
    int _y = x_range * (p.y - min_y);
    times_visited[_x + _y] += 1;
  }
  int visited_once = 0;
  for (int i = 0; i < num_positions; i++) {
    if (times_visited[i]) {
      visited_once += 1;
    }
  }
  free(times_visited);
  return visited_once;
}

int single_santa_visits(Array steps) {
  int x = 0, y = 0;
  Array positions;
  positions.length = 0;
  positions.items = calloc(steps.length + 1, sizeof(position));
  position *all_positions = (position*)positions.items;
  all_positions[positions.length++] = (position){x, y};
  for (int i = 0; i < steps.length; i++) {
    switch (((direction*)steps.items)[i]) {
      case up: y += 1; break;
      case down: y -= 1; break;
      case right: x += 1; break;
      case left: x -= 1; break;
    }
    all_positions[positions.length++] = (position){x, y};
  }
  int count = count_visits(positions);
  array_free(positions);
  return count;
}

int double_santa_visits(Array steps) {
  int santa_x = 0, santa_y = 0;
  int robo_x = 0, robo_y = 0;
  Array positions;
  positions.length = 0;
  positions.items = calloc(steps.length + 1, sizeof(position));
  position *all_positions = (position*)positions.items;
  all_positions[positions.length++] = (position){santa_x, santa_y};
  int real_santa = 1;
  for (int i = 0; i < steps.length; i++) {
    int x = real_santa ? santa_x : robo_x;
    int y = real_santa ? santa_y : robo_y;
    switch (((direction*)steps.items)[i]) {
      case up: y += 1; break;
      case down: y -= 1; break;
      case right: x += 1; break;
      case left: x -= 1; break;
    }
    all_positions[positions.length++] = (position){x, y};
    if (real_santa) {
      santa_x = x;
      santa_y = y;
    } else {
      robo_x = x;
      robo_y = y;
    }
    real_santa ^= 1;
  }
  int count = count_visits(positions);
  array_free(positions);
  return count;
}

int main() {
  Array steps = parse_steps();
  printf("houses visited with one santa: %d\n", single_santa_visits(steps));
  printf("houses visited with two santas: %d\n", double_santa_visits(steps));
  array_free(steps);
}
