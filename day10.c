#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define PUZZLE_INPUT "1113122113"
#define ROUNDS 50
#define STR_LEN 10000000
#define DEBUG 0

void look_and_say(char *start, char *dest) {
  char *walk = start;
  int added = 0;
  char c = walk[0];
  int count = 1;
  do {
    if ((++walk)[0] == c) {
      count++;
    } else {
      added += sprintf(dest + added, "%d%c", count, c);
      count = 1;
    }
  } while ((c = walk[0]));
}

void swap(char **a, char **b) {
  char *tmp = *a;
  *a = *b;
  *b = tmp;
}

int main() {
  char *start = malloc(STR_LEN);
  char *tmp = malloc(STR_LEN);
  sprintf(start, PUZZLE_INPUT);
  for (int i = 0; i < ROUNDS; i++) {
    printf("Round %i\n", i);
    look_and_say(start, tmp);
    swap(&start, &tmp);
  }
  printf("Total length: %lu\n", strlen(start));
  free(start);
  free(tmp);
  return 0;
}
