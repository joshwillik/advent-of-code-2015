#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <openssl/md5.h>
#define PUZZLE_INPUT "iwrupvqb"
#define MD5_STRING_LENGTH 32
#define MAX_COUNTER_DIGITS 15

char hex_chars[] = "0123456789abcdef";
void md5_to_hex(unsigned char *bytes, char *out) {
  for (size_t i = 0; i < MD5_DIGEST_LENGTH; i++) {
    out[i*2] = hex_chars[bytes[i] / 16];
    out[i*2 + 1] = hex_chars[bytes[i] % 16];
  }
}

void str_reverse(char *str) {
  char *end = str + strlen(str) - 1;
  char aux;
  while (end > str) {
    aux = *end;
    *end = *str;
    *str = aux;
    end -= 1;
    str += 1;
  }
}

void int_to_string(int value, char *result) {
  char *out = result;
  do {
    *out = "0123456789"[value % 10];
    out += 1;
    value /= 10;
  } while (value);
  *out = '\0';
  str_reverse(result);
}


char* make_input_str(int i) {
  char *out = calloc(strlen(PUZZLE_INPUT) + MAX_COUNTER_DIGITS + 1, sizeof(char));
  strcat(out, PUZZLE_INPUT);
  char str_counter[MAX_COUNTER_DIGITS];
  int_to_string(i, str_counter);
  strcat(out, str_counter);
  return out;
}

void find_n_zeroes(int n) {
  unsigned char *hash = calloc(MD5_DIGEST_LENGTH, sizeof(unsigned char));
  for (int counter = 1;; counter++) {
    char *input = make_input_str(counter);
    MD5((unsigned char*)input, strlen(input), hash);
    free(input);
    char *md5_str = calloc(MD5_STRING_LENGTH + 1, sizeof(char));
    md5_to_hex(hash, md5_str);
    bool winner_found = true;
    for (int i = 0; i < n; i++) {
      if (md5_str[i] != '0') {
        winner_found = false;
        break;
      }
    }
    if (winner_found) {
      printf("WINNER: found %s after %i hashes\n", md5_str, counter);
      free(md5_str);
      break;
    }
    free(md5_str);
  }
}

int main() {
  find_n_zeroes(5);
  find_n_zeroes(6);
  return 0;
}
