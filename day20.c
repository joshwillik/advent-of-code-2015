#include <stdio.h>
#include <stdbool.h>
#define TARGET_PRESENTS 36000000
#define LAZY_ELF_LIMIT 50

size_t num_visits(size_t n) {
  size_t sum = 1;
  size_t i = 1;
  while (i * i <= n) {
    if (n % i == 0) {
      sum += i;
      sum += n / i;
    }
    i++;
  }
  return sum;
}

size_t num_visits_lazy(size_t n) {
  size_t sum = 1;
  size_t i = 1;
  while (i * i <= n) {
    if (n % i == 0) {
      if (n <= (i * LAZY_ELF_LIMIT)) {
        sum += i;
      }
      if (n <= ((n/i) * LAZY_ELF_LIMIT)) {
        sum += n / i;
      }
    }
    i++;
  }
  return sum;
}

int main() {
  size_t house_i = 0;
  size_t house_1 = 0;
  size_t house_2 = 0;
  while (!(house_1 && house_2)) {
    house_i++;
    size_t house_1_presents = num_visits(house_i) * 10;
    if (house_1_presents >= TARGET_PRESENTS && !house_1) {
      house_1 = house_i;
    }
    size_t house_2_presents = num_visits_lazy(house_i) * 11;
    if (house_2_presents >= TARGET_PRESENTS && !house_2) {
      house_2 = house_i;
    }
  }
  printf("First house for normal elves is %zu\n", house_1);
  printf("First house for lazy elves is %zu\n", house_2);
  return 0;
}
