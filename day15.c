#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define PUZZLE_INPUT "day15.txt"
#define MAX_LINE 128
#define MAX_INGREDIENTS 10
#define COOKIE_SIZE 100
#define CLAMP(v) (((v) < 1) ? 1 : (v))
#define CALORIES 500

typedef struct Score {
  long long capacity;
  long long durability;
  long long flavor;
  long long texture;
  long long calories;
} Score;

typedef struct Ingredient {
  char name[30];
  Score score;
} Ingredient;

size_t parse_ingredients(Ingredient *ingredient, size_t max) {
  FILE *f = fopen(PUZZLE_INPUT, "r");
  char line[MAX_LINE];
  size_t parsed = 0;
  while (fgets(line, MAX_LINE, f)) {
    if (parsed == max) {
      fprintf(stderr, "Input too large to handle\n");
      exit(1);
    }
    Ingredient *i = ingredient + parsed;
    int tokens = sscanf(line,
      "%s capacity %lli, durability %lli, flavor %lli, texture %lli, calories %lli",
      i->name, &i->score.capacity, &i->score.durability,
      &i->score.flavor, &i->score.texture, &i->score.calories);
    if (tokens != 6) {
      fprintf(stderr, "Only got %i tokens from line: %s\n", tokens, line);
      exit(1);
    }
    i->name[strlen(i->name) - 1] = '\0'; /* truncate ':' from the end */
    parsed++;
  }
  fclose(f);
  return parsed;
}

Score score_add(Score a, Score b) {
  return (Score) {
    a.capacity + b.capacity,
    a.durability + b.durability,
    a.flavor + b.flavor,
    a.texture + b.texture,
    a.calories + b.calories,
  };
}

long long score_sum(Score a) {
  return CLAMP(a.capacity) * CLAMP(a.durability) * CLAMP(a.flavor) *
    CLAMP(a.texture);
}

void print_score(Score a) {
  printf("Score: %lli, capacity=%lli durability=%lli flavor=%lli texture=%lli\n",
    score_sum(a), a.capacity, a.durability, a.flavor, a.texture);
}

Score score_multiply(Score a, unsigned int n) {
  return (Score) {
    a.capacity * n,
    a.durability * n,
    a.flavor * n,
    a.texture * n,
    a.calories * n,
  };
}

int score_cmp(Score a, Score b) {
  long long _a = score_sum(a);
  long long _b = score_sum(b);
  long long diff = _a - _b;
  return diff > 0 ?
    1 :
    diff < 0 ?
      -1 :
      0;
}

Score best_score(Ingredient *ingredients, size_t n_ingredients,
  unsigned int teaspoons_left, Score prev)
{
  if (!n_ingredients) {
    return prev;
  }
  Score best = prev;
  for (size_t i = n_ingredients == 1 ? teaspoons_left : 0; i <= teaspoons_left; i++) {
    Score score = score_add(prev, score_multiply(ingredients[0].score, i));
    Score next = best_score(ingredients + 1, n_ingredients - 1,
      teaspoons_left - i, score);
    if (next.calories == CALORIES && score_cmp(next, best) == 1) {
      best = next;
    }
  }
  return best;
}

int main() {
  Ingredient ingredients[MAX_INGREDIENTS];
  size_t n_ingredients = parse_ingredients(ingredients, MAX_INGREDIENTS);
  Score score = best_score(ingredients, n_ingredients, COOKIE_SIZE, (Score){0});
  printf("The best possible score with 500 calories is: %lli\n",
    score_sum(score));
  return 0;
}
