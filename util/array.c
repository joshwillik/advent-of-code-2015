#include <stdint.h>

typedef struct {
  size_t length;
  size_t max;
  intptr_t *items;
} Array;

Array* array_new() {
  Array *a = calloc(1, sizeof(Array));
  return a;
}

void array_push(Array *a, intptr_t item) {
  if (a->length == a->max) {
    a->max += 100;
    a->items = realloc(a->items, sizeof(intptr_t) * a->max);
  }
  a->items[a->length++] = item;
}

Array *array_clone(Array *a) {
  Array *b = array_new();
  for (size_t i = 0; i < a->length; i++) {
    array_push(b, a->items[i]);
  }
  return b;
}

void array_swap(Array *arr, size_t a, size_t b) {
  intptr_t temp = arr->items[a];
  arr->items[a] = arr->items[b];
  arr->items[b] = temp;
}

void array_free_ignore(void *p) {
  (void)p;
}

void array_free(Array *a, void (*custom_free)(void*)) {
  void (*_free)(void *p) = custom_free != NULL ? custom_free : free;
  for (size_t i = 0; i < a->length; i++) {
    _free((void*) a->items[i]);
  }
  free(a->items);
  free(a);
}
